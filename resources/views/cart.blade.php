<x-guest-layout>
    @if (empty($cart))
        <p>the cart is empty</p>
    @else
        @foreach ($cart as $product)
            <div class="mx-4 my-4 p-4 rounded-lg bg-gray-50 text-black shadow-lg hover:shadow-inner grid grid-cols-2">
                <div>
                    <p>{{$product['name']}}</p>
                    <img class="h-24" src="{{$product['image']}}" alt="">
                </div>

                    <div class="  ">
                        <p>{{$product['description']}}</p>

                        <p>{{$product['price']*$product['quantity']}} €</p>
                        <form action="{{route('updatecart')}}" method="post">
                            @csrf
                            <input type="hidden" value="{{$product['id']}}" name="id">
                            <input type="number" value="{{$product['quantity']}}" name="quantity">
                            <button type="submit" >Update cart</button>
                        </form>
                        <form action="{{route('deleteitem')}}" method="post">
                            @csrf
                            <input type="hidden" value="{{$product['id']}}" name="id">
                            <button type="submit">delete</button>
                        </form>
                    </div>

            </div>
        @endforeach
    @endif

    <div class="w-96 h-36 flex-col border-2">
        <input id="card-holder-name" type="text" placeholder="Cardholder name">

    <!-- Stripe Elements Placeholder -->
    <div class="p-2" id="card-element"></div>

    <button class=" bg-blue-400 h-12 px-4 text-white text-xl rounded " id="card-button">
        Process Payment
    </button>
    </div>
</x-guest-layout>
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        const stripe = Stripe('pk_test_51KH680D3nCq6DYhoXfwKbNWmwRNrpoR2raj3NJHNsk2YrmxLwwewWsq5l5fVbeZAQDulIXndsrAdyQB7IjHYEWkj00gEv8unHZ');

        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#card-element');

        const cardHolderName = document.getElementById('card-holder-name');
const cardButton = document.getElementById('card-button');

cardButton.addEventListener('click', async (e) => {
    const { paymentMethod, error } = await stripe.createPaymentMethod(
        'card', cardElement, {
            billing_details: { name: cardHolderName.value }
        }
    );

    if (error) {
        console.log(error);
    } else {
        axios.post('/subscribe',{payment_method: paymentMethod.id})
        .then((data)=>{
            stripe.confirmCardPayment(data.data.paymentIntent.client_secret,
            {
                payment_method: {
                    card: cardElement
                }
            })
        });
    };
});
    </script>
